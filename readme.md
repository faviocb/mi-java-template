# Run source code

```
gradle clean build run
```

# Build jar and run it

```
gradle clean build jar; \
cd build/distributions; \
if [ -d "replaceme" ]; then
 rm -R replaceme
fi; \
tar -xvf replaceme.tar; \
cd replaceme; \
./bin/replaceme; \
cd ../../..
```
